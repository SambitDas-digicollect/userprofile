//
//  AddressViewController.swift
//  UserProfile
//
//  Created by Sambit Das on 23/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class AddressViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

}
extension AddressViewController : IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Address")
    }
}
