//
//  EmployeeViewController.swift
//  UserProfile
//
//  Created by Sambit Das on 28/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class EmployeeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
    @IBOutlet weak var collectionView: UICollectionView!
    
    var nameArray = ["David james","Lilly","Mark","sanjay","sambit","nithin","vinod","Raj"]
    var postArray = ["CTO","tech Support","CFO","CTO","ios Dev","ios Dev","ios Dev","System Admin"]

    override func viewDidLoad() {
        super.viewDidLoad()

//       let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
//       layout.minimumInteritemSpacing = 10
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("width : \(self.view.bounds.width) height : \(self.view.bounds.height)")
    }
     override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.reloadData()
        //print("rotate")
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameArray.count
    }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! EmployeeCVCell
          cell.resendButton.isHidden = true
        if title == "Rejected Invites"{
            cell.resendButton.isHidden = false
            cell.resendButton.layer.cornerRadius = 10
        }
        cell.EmployeeName.text = nameArray[indexPath.row]
        cell.EmployeePost.text = postArray[indexPath.row]
        cell.profileImage.image = #imageLiteral(resourceName: "userProfile")
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height / 2
        return cell
       }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(UIDevice.current.userInterfaceIdiom == .phone){
            
            if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
                        print("iPhone Landscape Left")
                return CGSize(width: collectionView.bounds.width / 2 - 10 , height: 100)
                    }
            else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight{
                        print("iPhone Landscape Right")
                print(collectionView.bounds.width / 2)
                print(collectionView.bounds.height / 2)
                return CGSize(width: collectionView.bounds.width / 2 - 10, height: 100)
                    }
            else if UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown{
                       print("iPhone Portrait Upside Down")
                return CGSize(width: collectionView.bounds.width - 10, height: 100)
                    }
            else if UIDevice.current.orientation == UIDeviceOrientation.portrait {
                        print("iPhone Portrait")
                return CGSize(width: collectionView.bounds.width - 10, height: 100)
            }else{return CGSize(width: collectionView.bounds.width - 10, height: 100)}
            
        }else{
            if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
                        print("iPad Landscape Left")
                return CGSize(width: 500, height: 100)
                    }
            else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight{
                        print("iPad Landscape Right")
                return CGSize(width: 500, height: 100)
                    }
            else if UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown{
                       print("iPad Portrait Upside Down")
                return CGSize(width: 400, height: 100)
                    }
            else if UIDevice.current.orientation == UIDeviceOrientation.portrait {
                        print("iPad Portrait")
                return CGSize(width: 400, height: 100)
            }else{return CGSize(width: 400, height: 100) }
        }
    }
    
}

extension EmployeeViewController : IndicatorInfoProvider{
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: self.title)
        }
}
