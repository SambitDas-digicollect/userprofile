//
//  RootViewController.swift
//  UserProfile
//
//  Created by Sambit Das on 28/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class RootViewController: ButtonBarPagerTabStripViewController {

    @IBOutlet weak var productButton: UIButton!
    @IBOutlet weak var employeeButton: UIButton!
    @IBOutlet weak var informationButton: UIButton!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var qrCodeImgView: UIImageView!
    
    override func viewDidLoad() {
        
        loadDesign()
        super.viewDidLoad()
        followButton.layer.cornerRadius = 10
        informationButton.layer.cornerRadius = 10
        informationButton.layer.borderWidth = 1
        informationButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        employeeButton.layer.cornerRadius = 10
        employeeButton.layer.borderWidth = 1
        employeeButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        productButton.layer.cornerRadius = 10
        productButton.layer.borderWidth = 1
        productButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        // code for QRCode
        let image = generateQRCode(from: "Digicollect.com")
        qrCodeImgView.image = image
        
    }
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child1 = UIStoryboard(name: "Main" , bundle: nil).instantiateViewController(identifier: "employeeView")
        child1.title = "All Employees"
         let child2 = UIStoryboard(name: "Main" , bundle: nil).instantiateViewController(identifier: "employeeView")
               child2.title = "Pending Invites"
        let child3 = UIStoryboard(name: "Main" , bundle: nil).instantiateViewController(identifier: "employeeView")
               child3.title = "Rejected Invites"
        return [child1, child2, child3]
    }

    func loadDesign(){
           self.settings.style.buttonBarItemBackgroundColor = #colorLiteral(red: 0.07450980392, green: 0.5019607843, blue: 0.8941176471, alpha: 1)
           self.settings.style.selectedBarBackgroundColor = .white
           self.settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
           self.settings.style.selectedBarHeight = 2
           //self.settings.style.buttonBarMinimumLineSpacing = 0
           self.settings.style.buttonBarItemTitleColor = .white
           self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.style.buttonBarItemLeftRightMargin =  0
        
           
           changeCurrentIndexProgressive = {(oldCell : ButtonBarViewCell?, newCell : ButtonBarViewCell?, progressPercentage : CGFloat, changeCurrentIndex : Bool, animated : Bool) -> Void in
               
               guard changeCurrentIndex == true else {return }
               oldCell?.label.textColor = UIColor.white
               newCell?.label.textColor = UIColor.white
           }
       }

}
