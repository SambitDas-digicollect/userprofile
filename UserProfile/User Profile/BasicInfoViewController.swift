//
//  BasicInfoViewController.swift
//  UserProfile
//
//  Created by Sambit Das on 23/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class BasicInfoViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var TopTitleName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if title == "BasicInfo"{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! BasicInfoTCell
            self.TopTitleName.text = "Basic Info"
            return cell
        }else if title == "Communication"{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! CommunicationCell
            self.TopTitleName.text = "Communication Information"
            return cell
        }else if title == "Address"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! AddressTCell
            self.TopTitleName.text = "Current Address"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as! PreferenceTCell
            self.TopTitleName.text = "Preference"
            return cell
        }
    }


}
class underLineStackView : UIStackView{
        
    override func  awakeFromNib() {
        super.awakeFromNib()
        
        //self.borderStyle = .none
        let btmBorder = UIView(frame: .zero)
                          btmBorder.backgroundColor = .white
                          self.addSubview(btmBorder)
                   btmBorder.translatesAutoresizingMaskIntoConstraints = false
                          btmBorder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
                          btmBorder.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
                          btmBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
                          btmBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 2).isActive = true
    
                  self.layer.masksToBounds = true
        
    }

}

extension BasicInfoViewController : IndicatorInfoProvider{
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: self.title)
        }
}
