//
//  ViewController.swift
//  UserProfile
//
//  Created by Sambit Das on 23/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MainViewController : ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var QRCodeImage: UIImageView!
    @IBOutlet weak var collectionViewForTab: ButtonBarView!

    override func viewDidLoad() {
        loadDesign()
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let image = generateQRCode(from: "Digicollect.com")
        QRCodeImage.image = image
        
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
     
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child1 = UIStoryboard(name: "Main" , bundle: nil).instantiateViewController(identifier: "")
        child1.title = "BasicInfo"
         let child2 = UIStoryboard(name: "Main" , bundle: nil).instantiateViewController(identifier: "BasicInfo")
               child2.title = "Communication"
        let child3 = UIStoryboard(name: "Main" , bundle: nil).instantiateViewController(identifier: "BasicInfo")
               child3.title = "Address"
        let child4 = UIStoryboard(name: "Main" , bundle: nil).instantiateViewController(identifier: "BasicInfo")
               child4.title = "Preference"
        return [child1, child2, child3, child4]
    }

    func loadDesign(){
        
        self.settings.style.selectedBarHeight = 1
        self.settings.style.selectedBarBackgroundColor = UIColor.white
        //self.settings.style.buttonBarBackgroundColor = .systemBlue
        self.settings.style.buttonBarItemBackgroundColor = #colorLiteral(red: 0.07450980392, green: 0.5019607843, blue: 0.8941176471, alpha: 1)
        self.settings.style.selectedBarBackgroundColor = .white
        self.settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        self.settings.style.selectedBarHeight = 2.0
        self.settings.style.buttonBarMinimumLineSpacing = 0
        self.settings.style.buttonBarItemTitleColor = .white
       // self.settings.style.buttonBarLeftContentInset = 0
        //self.settings.style.buttonBarRightContentInset = 0
        self.settings.style.buttonBarItemsShouldFillAvailableWidth = false
        
        changeCurrentIndexProgressive = {(oldCell : ButtonBarViewCell?, newCell : ButtonBarViewCell?, progressPercentage : CGFloat, changeCurrentIndex : Bool, animated : Bool) -> Void in
            
            guard changeCurrentIndex == true else {return }
            oldCell?.label.textColor = UIColor.white
            newCell?.label.textColor = UIColor.white
        }
    }
    
}

